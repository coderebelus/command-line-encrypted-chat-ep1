
// get standard input object
var readline = require('readline');
process.stdin.setEncoding('utf-8');
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// connect to server
var serverSocket = require('socket.io-client')('http://localhost:3000');

// clear console and write message
process.stdout.write('\033c');
process.stdout.cursorTo(0);
process.stdout.write('Enter a command to begin...\n');

// listen for command line input
rl.on('line', function(input) {
    // exit command
    if(input === 'exit\n') {
        process.exit();
    }
    // change username
    else if(input.indexOf('/change_username') !== -1) {
        var newUsername = input.replace('/change_username ', '').replace('\n', '');
        serverSocket.emit('onUsernameChange', {
            username: newUsername
        });
    }
    // send message
    else {
        var message = input.replace('\n', '');
        serverSocket.emit('onMessage', {
            message: message
        });
    }
});

// on successful connection to server
// serverSocket.on('connect', function() {
//     console.log('Connected to server...');
// });

// on handshake from server - give initialization details
// serverSocket.on('handshake', function(data) {

// });

// on username change from server
serverSocket.on('onUsernameChange', function(data) {
    console.log(data.oldUsername + ': changed username to ' + data.newUsername + '!');
});

// on message from server
serverSocket.on('onMessage', function(data) {
    console.log(data.username + ': ' + data.payload.message);
});

// on is typing from server
// serverSocket.on('isTyping', function(data) {
//     console.log(data.userName + ' is typing...');
// });

// on client disconnect from server
serverSocket.on('onDisconnect', function(data) {
    console.log(data.username + ': disconnected!');
});

// on disconnect from server
serverSocket.on('disconnect', function() {
    console.log('Disconnected from server...');
});