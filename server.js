var express = require('express'),
        app = express(),
        http = require('http').Server(app);
        io  = require('socket.io')(http);

app.get('/', function(req, res) {
    res.send('<h1>Landing page...</h1>');
});

var users = {};

// listen for connection
io.on('connection', function(socket) {
    console.log('New connection (' + socket.id + ')');
    onConnected(socket)

    // when change user name from client
    socket.on('onUsernameChange', function(data) {
        onUsernameChange(socket, data);
    });

    // when a message is sent from client
    socket.on('onMessage', function(data) {
        onMessage(socket, data);
    });

    // when a user is typing
    // socket.on('isTyping', function(data) {
    //     onIsTyping(socket, data);
    // });

    // when a user is disconnected
    socket.on('disconnect', function() {
        onDisconnect(socket);
    });
});


// on connected
function onConnected(socket) {
    users[socket.id] = {
        socketId: socket.id,
        username: 'Guest'
    };
}

// on username change
function onUsernameChange(socket, data) {
    var oldUsername = users[socket.id].username;
    users[socket.id].username = data.username;

    var dataToSend = {
        oldUsername: oldUsername,
        newUsername: data.username
    };
    socket.emit('onUsernameChange', dataToSend)
    socket.broadcast.emit('onUsernameChange', dataToSend);
}

// on message
function onMessage(socket, data) {
    var dataToSend = {
        socketId: socket.id,
        username: users[socket.id].username,
        payload: data
    }

    socket.emit('onMessage', dataToSend);
    socket.broadcast.emit('onMessage', dataToSend);
    console.log('(' + socket.id + ') new message: ' + data.message);
}

// on is typing
// function onIsTyping(socket, data) {
//     console.log('(' + socket.id + ') is typing');
// }

// on user disconnect
function onDisconnect(socket) {
    socket.broadcast.emit('onDisconnect', {
        username: users[socket.id].username
    });
    delete users[socket.id];

    console.log('disconnected (' + socket.id + ')');
}


// start listening
http.listen(3000, function() {
    console.log('listening for connection on port 3000');
});